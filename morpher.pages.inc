<?php

/**
 * @file
 * Menu page callbacks of Morpher module.
 */

/**
 * Settings form of the module.
 */
function morpher_admin_settings_form() {
  $form = array();

  $settings = morpher_settings();

  $form['morpher_settings'] = array(
    '#tree' => TRUE,
  );

  $form['morpher_settings']['wsdl'] = array(
    '#type' => 'textfield',
    '#title' => t('Service WSDL'),
    '#required' => TRUE,
    '#default_value' => isset($settings['wsdl']) ? $settings['wsdl'] : 'http://morpher.ru/WebService.asmx?WSDL',
  );

  $form['morpher_settings']['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => isset($settings['username']) ? $settings['username'] : '',
    '#description' => t('If you have an account in morpher.ru, enter username here.'),
  );

  $form['morpher_settings']['password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => isset($settings['password']) ? $settings['password'] : '',
    '#description' => t('If you have an account in morpher.ru, enter password here. <b>Attention:</b> this password will be stored as open text in the database.'),
  );

  $form['corrections'] = array(
    '#type' => 'item',
    '#title' => t('Correct'),
    '#value' => t('If you found any errors in morpher.ru service, please, report them <a href="!url">here</a>.', array(
      '!url' => 'http://morpher.ru/Correct.aspx',
    )),
  );

  return system_settings_form($form);
}
