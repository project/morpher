<?php

/**
 * @file
 * Provides API for declension of Russian words using http://morpher.ru API.
 */

/**
 * Denote nominative case in Russian grammar.
 *
 * @var string
 */
define('MORPHER_CASE_NOMINATIVE', 'И');

/**
 * Denote genitive case in Russian grammar.
 *
 * @var string
 */
define('MORPHER_CASE_GENITIVE', 'Р');


/**
 * Denote dative case in Russian grammar.
 *
 * @var string
 */
define('MORPHER_CASE_DATIVE', 'Д');

/**
 * Denote accusative case in Russian grammar.
 *
 * @var string
 */
define('MORPHER_CASE_ACCUSATIVE', 'В');

/**
 * Denote instrumental case in Russian grammar.
 *
 * @var string
 */
define('MORPHER_CASE_INSTRUMENTAL', 'Т');

/**
 * Denote prepositional case in Russian grammar.
 *
 * @var string
 */
define('MORPHER_CASE_PREPOSITIONAL', 'П');

/**
 * Denote prepositional case in Russian grammar, appended "about" word, which
 * can take different forms, according to Russian grammar, eg.: "о небесах",
 * "об облаках".
 *
 * @var string
 */
define('MORPHER_CASE_PREPOSITIONAL_ABOUT', 'П-о');

/**
 * Denote nominative case in Ukrainian grammar.
 *
 * @var string
 */
define('MORPHER_CASE_UK_NOMINATIVE', 'Н');

/**
 * Denote genitive case in Ukrainian grammar.
 *
 * @var string
 */
define('MORPHER_CASE_UK_GENITIVE', 'Р');

/**
 * Denote dative case in Ukrainian grammar.
 *
 * @var string
 */
define('MORPHER_CASE_UK_DATIVE', 'Д');

/**
 * Denote accusative case in Ukrainian grammar.
 *
 * @var string
 */
define('MORPHER_CASE_UK_ACCUSATIVE', 'З');

/**
 * Denote instrumental case in Ukrainian grammar.
 *
 * @var string
 */
define('MORPHER_CASE_UK_INSTRUMENTAL', 'О');

/**
 * Denote prepositional case in Ukrainian grammar.
 *
 * @var string
 */
define('MORPHER_CASE_UK_PREPOSITIONAL', 'М');

/**
 * Denote vocative case in Ukrainian grammar.
 *
 * @var string
 */
define('MORPHER_CASE_UK_VOCATIVE', 'К');

/**
 * Denote gender property name, used in some responses of SOAP server, when
 * working with Russian language.
 *
 * @var string
 */
define('MORPHER_GENDER', 'род');

/**
 * Denote gender property name, used in some responses of SOAP server, when
 * working with Ukrainian language.
 *
 * @var string
 */
define('MORPHER_GENDER_UK', 'рід');

/**
 * Denote plural property name, used in some responses of SOAP server.
 *
 * @var string
 */
define('MORPHER_PLURAL', 'множественное');

/**
 * Denote property name, where word is declined to answer the question "Where?".
 *
 * @var string
 */
define('MORPHER_WHERE', 'где');

/**
 * Denote property name, where word is declined to answer the question "Where
 * to?".
 *
 * @var string
 */
define('MORPHER_WHERE_TO', 'куда');

/**
 * Denote property name, where word is declined to answer the question "Where
 * from?".
 *
 * @var string
 */
define('MORPHER_WHERE_FROM', 'откуда');

/**
 * Implements hook_menu().
 */
function morpher_menu() {
  $items = array();

  $items['admin/settings/morpher'] = array(
    'title' => 'Morpher Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('morpher_admin_settings_form'),
    'access arguments' => array('administer morpher'),
    'file' => 'morpher.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_perm().
 */
function morpher_perm() {
  return array('administer morpher');
}

/**
 * Implements hook_flush_caches().
 */
function morpher_flush_caches() {
  return array('cache_morpher');
}

/**
 * Decline a string to grammar cases.
 *
 * @param string $string
 *   String to be declined to grammar cases. This string should be in
 *   nominative case
 * @param string $language
 *   Language of the $string argument. Allowed values are:
 *   - ru: Russian language
 *   - uk: Ukrainian language
 *
 * @return object|bool
 *   Object, returned from morpher.ru SOAP API, you may use constants
 *   MORPHER_* as property names of this object
 *   If the operation has failed FALSE will be returned
 */
function morpher_declension($string, $language = 'ru') {
  $param = array('parameters' => array(
    's' => $string,
  ));

  $method = 'GetXml';
  if ($language == 'uk') {
    $method .= 'Ukr';
  }
  $response = morpher_soap_invoke($method, $param);

  if (!isset($response->{$method . 'Result'})) {
    return FALSE;
  }

  return $response->{$method . 'Result'};
}

/**
 * Spell out a quantity of units in words.
 *
 * @param int $quantity
 *   Amount of units
 * @param string $units
 *   Name of units in nominative case in UTF-8 encoding
 * @param string $language
 *   Language of the $units argument. Allowed values are:
 *   - ru: Russian language
 *   - uk: Ukrainian language
 *
 * @return object|bool
 *   Object with the following structure:
 *   - n: (object) spelled out in words "$quantity $units" in different grammar
 *     cases
 *   - unit: (object) spelled out in words "$units" (without the quantitative
 *     part) in different grammar cases
 *   If the operation has failed FALSE will be returned
 */
function morpher_spell_out($quantity, $units, $language = 'ru') {
  $param = array('parameters' => array(
    'n' => intval($quantity),
    'unit' => $units,
  ));

  $method = 'Propis';
  if ($language == 'uk') {
    $method .= 'Ukr';
  }

  $result = morpher_soap_invoke($method, $param);
  if (!isset($result->{$method . 'Result'})) {
    // Seems like the request failed.
    return FALSE;
  }

  return $result->{$method . 'Result'};
}

/**
 * Retrieve settings of the module.
 *
 * @return array
 *   Array of settings, has the following structure:
 *   - wsdl: (string) URL of the WSDL description of the service that provides
 *     declension
 *   - username: (string) a morpher.ru username to use, when requesting remote
 *     SOAP server (if any)
 *   - password: (string) a morpher.ru password to use, when requesting remote
 *     SOAP server (if any)
 */
function morpher_settings() {
  return variable_get('morpher_settings', array());
}

/**
 * Supportive function.
 *
 * Invoke a SOAP method of remote service http://morpher.ru/.
 *
 * @param string $method_name
 *   Name of the remote method to invoke
 * @param array $params
 *   Array of arguments to pass on to the remote method
 *
 * @return mixed
 *   Return whatever the remote method has returned
 */
function morpher_soap_invoke($method_name, $params = array()) {
  // Checking cache firstly.
  $cid = $method_name;
  if (!empty($params))  {
    $cid .= '_' . md5(json_encode($params));
  }
  $cache = cache_get($cid, 'cache_morpher');

  if (isset($cache->data)) {
    return $cache->data;
  }

  // No cache was found, we will make a request to remote Soap server.
  $settings = morpher_settings();

  try {
    $soap = new SoapClient($settings['wsdl'], array('exceptions' => TRUE));

    // Including credentials if such are available.
    if ($settings['username'] && $settings['password']) {
      $credentials = array(
        'Username' => $settings['username'],
        'Password' => $settings['password'],
      );
      $header = new SOAPHeader('http://morpher.ru/', 'Credentials', $credentials);
      $soap->__setSoapHeaders($header);
    }

    $response = $soap->__soapCall($method_name, $params);
  }
  catch (SoapFault $e) {
    watchdog('morpher', 'SoapFault exception in %method. Params: @params. Exception: @exception.', array(
      '%method' => $method_name,
      '@params' => print_r($params, 1),
      '@exception' => $e->getMessage(),
    ), WATCHDOG_ERROR);
    return NULL;
  }

  // Storing results of request in cache.
  cache_set($cid, $response, 'cache_morpher');

  return $response;
}
